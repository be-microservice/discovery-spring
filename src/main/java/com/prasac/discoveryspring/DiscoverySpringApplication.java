package com.prasac.discoveryspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DiscoverySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoverySpringApplication.class, args);
	}

}
